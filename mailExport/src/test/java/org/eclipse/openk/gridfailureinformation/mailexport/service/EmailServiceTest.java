/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.service;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.gridfailureinformation.mailexport.MailExportApplication;
import org.eclipse.openk.gridfailureinformation.mailexport.config.EmailConfig;
import org.eclipse.openk.gridfailureinformation.mailexport.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.mailexport.dtos.MailMessageDto;
import org.eclipse.openk.gridfailureinformation.mailexport.email.GfiEmail;
import org.eclipse.openk.gridfailureinformation.mailexport.support.MockDataHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = MailExportApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class EmailServiceTest {
    private static GreenMail mailServer;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmailConfig emailConfig;

    @BeforeAll
    public static void beforeAll() {
        ServerSetup serverSetup = new ServerSetup(3025 , "localhost", ServerSetup.PROTOCOL_SMTP);
        serverSetup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(serverSetup);
    }

    @BeforeEach
    public void beforeTest() {
        mailServer.start();
    }

    @AfterEach
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void testSendHtmlEmail() throws MessagingException {
        MailMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailService.sendMail(mailMessageDto);
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendEmail() throws MessagingException {
        MailMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailConfig.setHtmlEmail(false);
        emailService.sendMail(mailMessageDto);
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendTestEmail() throws MessagingException {
        MailMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailConfig.setHtmlEmail(false);
        emailConfig.setUseHtmlEmailTemplate(false);
        emailService.sendTestMail("tester@test.de");
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendMail_nok() {
        mailServer.stop();
        emailConfig.setUseHtmlEmailTemplate(false);
        MailMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        assertThrows(MessagingException.class, () -> emailService.sendMail(mailMessageDto));
    }

    @Test
    public void testSendMail_invalidRecipient() {
        MailMessageDto mailMessageDto = MockDataHelper.mockMailMessageDtoWrongRecipientFormat();
        assertThrows(MessagingException.class, () -> emailService.sendMail(mailMessageDto));
    }

    @Test
    public void testSendMail_invalidSender() {
        EmailConfig emailConfig = new EmailConfig();
        emailConfig.setSmtpHost("localhost");
        emailConfig.setEmailPort("3025");
        emailConfig.setSender("testCaseSendertest.de");
        assertThrows(MessagingException.class, () -> {
            GfiEmail emailManager = new GfiEmail(emailConfig);
            emailManager.sendEmail();
        });
    }
}
