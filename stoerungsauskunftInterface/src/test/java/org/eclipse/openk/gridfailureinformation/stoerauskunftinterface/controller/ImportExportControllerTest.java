/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import lombok.With;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.SecurityConfig;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class, SecurityConfig.class})
@ActiveProfiles("test")
public class ImportExportControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    @SpyBean
    private ImportExportService importExportService;

    @Autowired
    private Response testResponse;

    @SpyBean
    private StoerungsauskunftApi stoerungsauskunftApi;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallImport() throws Exception {
        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        mockMvc.perform(get("/stoerungsauskunft/usernotification-import-test"))
                .andExpect(status().is2xxSuccessful());

        verify(importExportService, times(1)).importUserNotifications();
    }

    @Test
    public void shouldCallImportAndReturnUnauthorized() throws Exception {
        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        mockMvc.perform(get("/stoerungsauskunft/usernotification-import-test"))
                .andExpect(status().isUnauthorized());

        verify(importExportService, times(0)).importUserNotifications();
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallExport() throws Exception {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        mockMvc.perform(post("/stoerungsauskunft/outage-export-test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(rabbitMqMessageDto)))
                .andExpect(status().is2xxSuccessful());

        verify(importExportService, times(1)).exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }

    @Test
    public void shouldCallExportAndReturnUnauthorized() throws Exception {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        mockMvc.perform(post("/stoerungsauskunft/outage-export-test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(rabbitMqMessageDto)))
                .andExpect(status().isUnauthorized());

        verify(importExportService, never()).exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }
}
