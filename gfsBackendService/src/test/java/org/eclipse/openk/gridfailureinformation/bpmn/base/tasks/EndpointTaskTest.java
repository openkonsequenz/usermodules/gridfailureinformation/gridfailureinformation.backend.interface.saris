/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.TestProcessSubject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EndpointTaskTest {
    @Test
    public void testEndpoint_enterStep() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        ep.enterStep(subject);
        assertNotNull(ep);
    }

    @Test
    public void testEndpoint_connectTo() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        assertThrows(ProcessException.class, () -> ep.connectOutputTo(null) ) ;
    }

    @Test
    public void testEndpoint_leaveStep() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        assertThrows(ProcessException.class, () -> ep.leaveStep(subject) ) ;
    }


    @Test
    public void testEndpoint_recover() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        assertThrows(ProcessException.class, () -> ep.recover(subject) ) ;
    }
}
