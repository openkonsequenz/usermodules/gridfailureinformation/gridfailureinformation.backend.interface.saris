/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import org.eclipse.openk.gridfailureinformation.bpmn.base.BaseTaskImpl;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ServiceTaskImpl;
import org.eclipse.openk.gridfailureinformation.bpmn.base.TestProcessSubject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ServiceTaskTest {

    @Test
    public void testServiceTask() throws ProcessException {
        ServiceTaskImpl service = new ServiceTaskImpl("MyService");
        BaseTaskImpl basetask = new BaseTaskImpl("Endpoint");
        TestProcessSubject subject = new TestProcessSubject();

        service.connectOutputTo(basetask);

        service.enterStep(subject);

        assertTrue( service.enterStepCalled );
        assertTrue( service.leaveStepCalled );
        assertTrue( basetask.enterStepCalled);
    }

    @Test
    public void testOnEnterStep() throws ProcessException {
        ServiceTaskImpl service = new ServiceTaskImpl("MyService");
        //BaseTaskImpl basetask = new BaseTaskImpl("Endpoint");
        TestProcessSubject subject = new TestProcessSubject();

        service.onEnterStep(subject);
    }

    @Test
    public void testOnRecover() throws ProcessException {
        ServiceTaskImpl service = new ServiceTaskImpl("MyService");
        TestProcessSubject subject = new TestProcessSubject();

        service.onRecover(subject);

        assertTrue( service.enterStepCalled);
    }

}
