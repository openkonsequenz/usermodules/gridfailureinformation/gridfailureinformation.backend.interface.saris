/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class DistributionGroupControllerTest {
    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    @SpyBean
    private DistributionGroupService distributionGroupService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new DistributionGroupController(distributionGroupService))
                .build();
    }

    @Test
    public void shouldReturnDistributionGroups() throws Exception {
        List<DistributionGroupDto> distributionGroupDtoList = MockDataHelper.mockDistributionGroupDtoList();
        doReturn(distributionGroupDtoList).when(distributionGroupService).getDistributionGroups();

        mockMvc.perform(get("/distribution-groups"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnARequestedDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        doReturn(groupDto).when(distributionGroupService).getDistributionGroupByUuid(any());

        mockMvc.perform(get("/distribution-groups/"+ UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("name", is(  groupDto.getName())));
    }

    @Test
    public void shouldExceptionIfNotFoundDistributionGroup() throws Exception {
        doThrow(new NotFoundException()).when(distributionGroupService).getDistributionGroupByUuid(any());

        mockMvc.perform(get("/distribution-groups/"+ UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldInsertDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        doReturn(groupDto).when(distributionGroupService).insertDistributionGroup(any());

        mockMvc.perform(post("/distribution-groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(jsonPath("$.name", is(groupDto.getName())))
                .andExpect(jsonPath("$.distributionTextPublish", is(groupDto.getDistributionTextPublish())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldUpdateDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        doReturn(groupDto).when(distributionGroupService).updateGroup(any());

        mockMvc.perform(put("/distribution-groups/{groupUuid}", groupDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldUpdateDistributionGroupDueToException() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        doReturn(groupDto).when(distributionGroupService).updateGroup(any());

        mockMvc.perform(put("/distribution-groups/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldDeleteDistributionGroup() throws Exception {
        TblDistributionGroup dto = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.findByUuid(any())).thenReturn(Optional.of(dto));

        mockMvc.perform(delete("/distribution-groups/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}
