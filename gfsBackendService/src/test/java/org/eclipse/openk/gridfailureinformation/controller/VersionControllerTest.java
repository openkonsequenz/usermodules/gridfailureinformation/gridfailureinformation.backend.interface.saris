/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.*;
import org.eclipse.openk.gridfailureinformation.service.VersionService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.VersionDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class VersionControllerTest {
    @Autowired
    @SpyBean
    private VersionService versionService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new VersionController(versionService))
                .build();
    }

    @Test
    public void shouldReturnVersion() throws Exception {
        VersionDto versionDto = MockDataHelper.mockVersionDto();

        doReturn(versionDto).when(versionService).getVersion();

        mockMvc.perform(get("/version"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("backendVersion", is("660")))
                .andExpect(jsonPath("dbVersion", is("550")));
    }

    @Test
    public void shouldRaiseBadRequest1() throws Exception {
        checkException(new BadRequestException(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldRaiseBadRequest2() throws Exception {
        checkException(new BadRequestException("test"), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldRaiseConflict() throws Exception {
        checkException(new ConflictException("test"), HttpStatus.CONFLICT);
    }

    @Test
    public void shouldRaiseInternalServer() throws Exception {
        checkException(new InternalServerErrorException("test"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldRaiseNotFound() throws Exception {
        checkException(new NotFoundException("test"), HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldRaiseNotAcceptable() throws Exception {
        checkException(new OperationDeniedException(OperationType.CREATE, "test"), HttpStatus.NOT_ACCEPTABLE);
    }

    @Test
    public void shouldRaiseUnautorized() throws Exception {
        checkException(new UnauthorizedException("test"), HttpStatus.UNAUTHORIZED);
    }

    private void checkException(Exception ex, HttpStatus codeToCheck) throws Exception {
        doThrow(ex).when(versionService).getVersion();

        mockMvc.perform(get("/version")).andExpect(status().is(codeToCheck.value()));
    }
}
