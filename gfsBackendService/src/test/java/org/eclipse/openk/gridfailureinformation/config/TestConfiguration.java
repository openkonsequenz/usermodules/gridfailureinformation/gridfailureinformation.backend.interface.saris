/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.email.MessageConsumer;
import org.eclipse.openk.gridfailureinformation.mapper.FailureClassificationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationDistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.RadiusMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StatusMapper;
import org.eclipse.openk.gridfailureinformation.mapper.VersionMapper;
import org.eclipse.openk.gridfailureinformation.repository.VersionRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureClassificationService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationDistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationReminderMailSentService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.eclipse.openk.gridfailureinformation.service.ImportService;
import org.eclipse.openk.gridfailureinformation.service.RadiusService;
import org.eclipse.openk.gridfailureinformation.service.StationService;
import org.eclipse.openk.gridfailureinformation.service.StatusService;
import org.eclipse.openk.gridfailureinformation.service.VersionService;
import org.eclipse.openk.gridfailureinformation.util.ImportDataValidator;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import feign.Request;
import feign.RequestTemplate;
import feign.Response;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.api.AuthNAuthApi;
import org.eclipse.openk.gridfailureinformation.api.ContactApi;
import org.eclipse.openk.gridfailureinformation.api.SitCacheApi;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiGrid;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqProperties;
import org.eclipse.openk.gridfailureinformation.email.EmailConfig;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.mapper.BranchMapper;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMemberMapper;
import org.eclipse.openk.gridfailureinformation.mapper.ExpectedReasonMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationPublicationChannelMapper;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.tools.VisibilityConfigurationMapper;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupAllowedRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupMemberRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.ExpectedReasonRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationReminderMailSentRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.AddressService;
import org.eclipse.openk.gridfailureinformation.service.AuthNAuthService;
import org.eclipse.openk.gridfailureinformation.service.BranchService;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupMemberService;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.DistributionTextPlaceholderService;
import org.eclipse.openk.gridfailureinformation.service.ExpectedReasonService;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.service.SettingsService;
import org.eclipse.openk.gridfailureinformation.util.GroupMemberPlzFilter;
import org.mapstruct.factory.Mappers;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.HashMap;

import static org.mockito.Mockito.doNothing;

@EnableJpaRepositories(basePackages = "org.eclipse.openk.gridfailureinformation")
@EntityScan(basePackageClasses = GridFailureInformationApplication.class)
@ContextConfiguration(initializers = {ConfigDataApplicationContextInitializer.class})
@org.springframework.boot.test.context.TestConfiguration
@TestPropertySource(locations = "classpath:application-test.yml")
public class TestConfiguration {
    @Value("${spring.rabbitmq.host}")
    private String rabbitMqHost;
    @Value("${spring.rabbitmq.username}")
    private String rabbitMqUsername;
    @Value("${spring.rabbitmq.password}")
    private String rabbitMqPassword;

    @MockBean
    private AddressRepository addressRepository;

    @Bean
    public AddressService addressService() {
        return new AddressService(addressRepository, Mappers.getMapper(AddressMapper.class));
    }

    @MockBean
    private BranchRepository branchRepository;

    @Bean
    public BranchService branchService() {
        return new BranchService(branchRepository, Mappers.getMapper(BranchMapper.class));
    }

    @MockBean
    private AuthNAuthApi authNAuthApi;

    @Bean
    public AuthNAuthService authNAuthService() {
        return new AuthNAuthService(authNAuthApi);
    }

    @Bean
    public Response testResponse() {
        var headers = new HashMap<String, Collection<String>>();
        var template = new RequestTemplate();
        var request = Request.create(Request.HttpMethod.GET, "/test", headers, Request.Body.empty(), template);
        return Response.builder().request(request).build();
    }

    @MockBean
    private DistributionGroupRepository distributionGroupRepository;

    @MockBean
    private DistributionGroupMemberRepository distributionGroupMemberRepository;

    @Bean
    public DistributionGroupMemberMapper distributionGroupMemberMapper() {
        return Mappers.getMapper(DistributionGroupMemberMapper.class);
    }

    @MockBean
    private ContactApi contactApi;

    @Bean
    public DistributionGroupMemberService distributionGroupMemberService() {
        return new DistributionGroupMemberService(
                distributionGroupMemberRepository,
                Mappers.getMapper(DistributionGroupMemberMapper.class),
                distributionGroupRepository,
                contactApi,
                authNAuthService()
        );
    }

    @MockBean
    private DistributionGroupAllowedRepository distributionGroupAllowedRepository;

    @Bean
    public DistributionGroupService distributionGroupService() {
        return new DistributionGroupService(
                distributionGroupRepository,
                distributionGroupAllowedRepository,
                Mappers.getMapper(DistributionGroupMapper.class),
                distributionGroupMemberRepository,
                distributionGroupAllowedRepository
        );
    }

    @Bean
    public DistributionTextPlaceholderService distributionTextPlaceholderService() {
        return new DistributionTextPlaceholderService();
    }

    @MockBean
    private FailureClassificationRepository failureClassificationRepository;

    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @MockBean
    private HistFailureInformationRepository histFailureInformationRepository;

    @MockBean
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @MockBean
    private FailureInformationStationRepository failureInformationStationRepository;

    @MockBean
    private HistFailureInformationStationRepository histFailureInformationStationRepository;

    @Bean
    public HistFailureInformationStationService histFailureInformationStationService() {
        return new HistFailureInformationStationService(
                stationRepository,
                Mappers.getMapper(StationMapper.class),
                histFailureInformationRepository,
                histFailureInformationStationRepository,
                failureInformationStationRepository,
                Mappers.getMapper(HistFailureInformationStationMapper.class)
        );
    }

    @MockBean
    private FailureInformationPublicationChannelRepository failureInformationPublicationChannelRepository;

    @MockBean
    private FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private RadiusRepository radiusRepository;

    @MockBean
    private ExpectedReasonRepository expectedReasonRepository;

    @Bean
    public ExpectedReasonService expectedReasonService() {
        return new ExpectedReasonService(
                expectedReasonRepository,
                Mappers.getMapper(ExpectedReasonMapper.class)
        );
    }

    @MockBean
    private StationRepository stationRepository;

    @Bean
    public GfiGrid gfiGrid() throws ProcessException {
        return new GfiGrid();
    }

    @Bean
    @Lazy
    public ProcessHelper processHelper() throws ProcessException {
        return new ProcessHelper(rabbitMqConfig(), gfiProcessEnvironment(), gfiGrid());
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public FailureInformationService failureInformationService() {
        return new FailureInformationService(
                failureInformationRepository,
                histFailureInformationRepository,
                Mappers.getMapper(FailureInformationMapper.class),
                Mappers.getMapper(FailureInformationPublicationChannelMapper.class),
                branchRepository,
                failureClassificationRepository,
                radiusRepository,
                expectedReasonRepository,
                statusRepository,
                stationRepository,
                addressRepository,
                failureInformationDistributionGroupRepository,
                failureInformationPublicationChannelRepository,
                failureInformationStationRepository,
                failureInformationReminderMailSentRepository,
                histFailureInformationStationService(),
                rabbitMqConfig(),
                objectMapper()
        );
    }

    @Bean
    public RabbitMqConfig rabbitMqConfig() {
        RabbitMqConfig rabbitMqConfig = new RabbitMqConfig(rabbitTemplate(), rabbitMqProperties());

        var rabbitMqConfigSpy = Mockito.spy(rabbitMqConfig);

        doNothing().when(rabbitMqConfigSpy).buildAllQueues();

        return rabbitMqConfigSpy;
    }

    @Bean
    public RabbitMqProperties rabbitMqProperties() {
        return new RabbitMqProperties();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        var factory = new CachingConnectionFactory();
        factory.setHost(rabbitMqHost);
        factory.setUsername(rabbitMqUsername);
        factory.setPassword(rabbitMqPassword);
        return new RabbitTemplate(factory);
    }

    @Bean
    public ResourceConfig resourceConfig() {
        return new ResourceConfig(distributionTextPlaceholderService());
    }

    @Bean
    public GroupMemberPlzFilter groupMemberPlzFilter() {
        return new GroupMemberPlzFilter(failureInformationStationRepository, addressRepository);
    }

    @MockBean
    private SitCacheApi sitCacheApi;

    @Bean
    public VisibilityConfig.FieldVisibility fieldVisibility() {
        return new VisibilityConfig.FieldVisibility();
    }

    @Bean
    public VisibilityConfig.TableInternColumnVisibility tableInternColumnVisibility() {
        return new VisibilityConfig.TableInternColumnVisibility();
    }

    @Bean
    public VisibilityConfig.TableExternColumnVisibility tableExternColumnVisibility() {
        return new VisibilityConfig.TableExternColumnVisibility();
    }

    @Bean
    public VisibilityConfig.MapExternTooltipVisibility mapExternTooltipVisibility() {
        return new VisibilityConfig.MapExternTooltipVisibility();
    }

    @Bean
    public VisibilityConfig visibilityConfig() {
        return new VisibilityConfig(
                fieldVisibility(),
                tableInternColumnVisibility(),
                tableExternColumnVisibility(),
                mapExternTooltipVisibility()
        );
    }

    @Bean
    public FESettings feSettings() {
        return new FESettings();
    }

    @Bean
    public SettingsService settingsService() {
        return new SettingsService(
                feSettings(),
                visibilityConfig(),
                rabbitMqProperties(),
                Mappers.getMapper(VisibilityConfigurationMapper.class)
        );
    }

    @Bean
    public ExportService exportService() {
        return new ExportService(
                rabbitMqConfig(),
                rabbitMqProperties(),
                rabbitTemplate(),
                failureInformationService(),
                failureInformationStationRepository,
                stationRepository,
                distributionGroupMemberService(),
                Mappers.getMapper(FailureInformationMapper.class),
                failureInformationRepository,
                statusRepository,
                failureInformationPublicationChannelRepository,
                objectMapper(),
                distributionGroupRepository,
                failureInformationDistributionGroupRepository,
                resourceConfig(),
                authNAuthService(),
                groupMemberPlzFilter(),
                sitCacheApi,
                settingsService(),
                restTemplate()
        );
    }

    @Bean
    public GfiProcessEnvironment gfiProcessEnvironment() {
        return new GfiProcessEnvironment(
                failureClassificationRepository,
                failureInformationRepository,
                statusRepository,
                failureInformationService(),
                Mappers.getMapper(FailureInformationMapper.class),
                failureInformationPublicationChannelRepository,
                exportService()
        );
    }

    @Bean
    public EmailConfig emailConfig() {
        return new EmailConfig();
    }

    @Bean
    public EmailService emailService() {
        return new EmailService(emailConfig());
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public FailureClassificationService failureClassificationService() {
        return new FailureClassificationService(
                failureClassificationRepository,
                Mappers.getMapper(FailureClassificationMapper.class)
        );
    }

    @Bean
    public FailureInformationDistributionGroupService failureInformationDistributionGroupService() {
        return new FailureInformationDistributionGroupService(
                distributionGroupRepository,
                Mappers.getMapper(DistributionGroupMapper.class),
                failureInformationRepository,
                failureInformationDistributionGroupRepository,
                Mappers.getMapper(FailureInformationDistributionGroupMapper.class)
        );
    }

    @Bean
    public FailureInformationReminderMailSentService failureInformationReminderMailSentService() {
        return new FailureInformationReminderMailSentService(
                failureInformationService(),
                failureInformationReminderMailSentRepository,
                failureInformationRepository,
                Mappers.getMapper(FailureInformationMapper.class),
                statusRepository,
                exportService()
        );
    }

    @Bean
    public FailureInformationStationService failureInformationStationService() {
        return new FailureInformationStationService(
                stationRepository,
                Mappers.getMapper(StationMapper.class),
                failureInformationRepository,
                failureInformationStationRepository,
                Mappers.getMapper(FailureInformationStationMapper.class)
        );
    }

    @Bean
    public HistFailureInformationService histFailureInformationService() {
        return new HistFailureInformationService(
                histFailureInformationRepository,
                Mappers.getMapper(HistFailureInformationMapper.class),
                histFailureInformationStationService()
        );
    }

    @Bean
    public StationMapper stationMapper() {
        return Mappers.getMapper(StationMapper.class);
    }

    @Bean
    public ImportGeoJsonService importGeoJsonService() {
        return new ImportGeoJsonService(stationRepository, objectMapper());
    }

    @Bean
    public RadiusService radiusService() {
        return new RadiusService(radiusRepository, Mappers.getMapper(RadiusMapper.class));
    }

    @Bean
    public StatusService statusService() {
        return new StatusService(statusRepository, Mappers.getMapper(StatusMapper.class));
    }

    @Bean
    public StationService stationService() {
        return new StationService(
                stationRepository,
                addressRepository,
                Mappers.getMapper(StationMapper.class),
                failureInformationService()
        );
    }

    @Bean
    public ImportDataValidator importDataValidator() {
        return new ImportDataValidator(objectMapper());
    }

    @Bean
    public ImportService importService() throws ProcessException {
        return new ImportService(
                rabbitMqConfig(),
                failureInformationService(),
                branchService(),
                radiusService(),
                statusService(),
                stationService(),
                addressRepository,
                Mappers.getMapper(FailureInformationMapper.class),
                importDataValidator(),
                processHelper(),
                objectMapper()
        );
    }

    @MockBean
    private VersionRepository versionRepository;

    @Bean
    public VersionService versionService() {
        return new VersionService(versionRepository, Mappers.getMapper(VersionMapper.class));
    }

    @Bean
    public MessageConsumer messageConsumer() {
        return new MessageConsumer(objectMapper(), emailService());
    }
}

