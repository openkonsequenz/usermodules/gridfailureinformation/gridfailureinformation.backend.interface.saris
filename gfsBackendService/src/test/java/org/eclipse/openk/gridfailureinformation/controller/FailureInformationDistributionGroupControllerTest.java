/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationDistributionGroupService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class FailureInformationDistributionGroupControllerTest {
    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    @SpyBean
    private FailureInformationDistributionGroupService failureInformationDistributionGroupService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new FailureInformationDistributionGroupController(failureInformationDistributionGroupService))
                .build();
    }

    @Test
    public void shouldReturnDistributionGroupsForFailureInfo() throws Exception {
        List<DistributionGroupDto> distributionGroupDtoList = MockDataHelper.mockDistributionGroupDtoList();

        doReturn(distributionGroupDtoList).when(failureInformationDistributionGroupService).findDistributionGroupsByFailureInfo(any());

        mockMvc.perform(get("/grid-failure-informations/" + UUID.randomUUID() + "/distribution-groups"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldAssignDistributionGroupToFailureInfo() throws Exception {
        FailureInformationDistributionGroupDto failureInfoGroupDto = MockDataHelper.mockFailureInformationDistributionGroupDto();

        doReturn(failureInfoGroupDto).when(failureInformationDistributionGroupService).insertFailureInfoDistributionGroup(any(UUID.class), any(DistributionGroupDto.class));

        mockMvc.perform(post("/grid-failure-informations/" + UUID.randomUUID() + "/distribution-groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoGroupDto)))
                .andExpect(jsonPath("failureInformationId", is(failureInfoGroupDto.getFailureInformationId().intValue())))
                .andExpect(jsonPath("distributionGroupId", is(failureInfoGroupDto.getDistributionGroupId().intValue())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldDeleteDistributionGroupForFailureInformation() throws Exception {
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        TblFailureInformationDistributionGroup tblFailureInformationDistributionGroup = MockDataHelper.mockTblFailureInformationDistributionGroup();

        when(failureInformationRepository.findByUuid(any())).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupRepository.findByUuid(any())).thenReturn(Optional.of(tblDistributionGroup));
        when(failureInformationDistributionGroupRepository.findByFailureInformationIdAndDistributionGroupId(any(), any())).thenReturn(Optional.of(tblFailureInformationDistributionGroup));

        mockMvc.perform(delete("/grid-failure-informations/" + UUID.randomUUID() + "/distribution-groups/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}
