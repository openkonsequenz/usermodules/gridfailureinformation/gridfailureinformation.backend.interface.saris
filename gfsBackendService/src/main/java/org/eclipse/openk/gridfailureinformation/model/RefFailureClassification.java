/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
public class RefFailureClassification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "REF_FAILURE_CLASS_ID_SEQ")
    @SequenceGenerator(name = "REF_FAILURE_CLASS_ID_SEQ", sequenceName = "REF_FAILURE_CLASS_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private String classification;
    private String description;
}
