package org.eclipse.openk.gridfailureinformation.viewmodel;

import lombok.Data;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupAllowed;

import java.util.UUID;

@Data
public class DistributionGroupAllowedDto {

    private long id = 0;
    private UUID distributionGroupId;
    private UUID branchId;
    private UUID classificationId;

    private boolean autoSet = false;

    private boolean deleted = false;

    public DistributionGroupAllowedDto() {}

    public DistributionGroupAllowedDto(TblDistributionGroupAllowed dbData) {

        id = dbData.getId();
        distributionGroupId = dbData.getDistributionGroupId();
        branchId = dbData.getBranchId();
        classificationId = dbData.getClassificationId();
        autoSet = dbData.isAutoSet();
    }
}
