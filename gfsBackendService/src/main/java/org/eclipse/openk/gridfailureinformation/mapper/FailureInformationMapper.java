/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper;

import org.eclipse.openk.gridfailureinformation.mapper.tools.AddressSplitterMerger;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ForeignFailureDataDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FailureInformationMapper {
    @Mappings({
            @Mapping(source = "refFailureClassification.uuid", target = "failureClassificationId"),
            @Mapping(source = "refFailureClassification.classification", target = "failureClassification"),
            @Mapping(source = "refStatusIntern.uuid", target = "statusInternId"),
            @Mapping(source = "refStatusIntern.status", target = "statusIntern"),
            @Mapping(source = "refBranch.uuid", target = "branchId"),
            @Mapping(source = "refBranch.name", target = "branch"),
            @Mapping(source = "refBranch.description", target = "branchDescription"),
			@Mapping(source = "refBranch.colorCode", target = "branchColorCode"),
            @Mapping(source = "refRadius.uuid", target = "radiusId"),
            @Mapping(source = "refRadius.radius", target = "radius"),
            @Mapping(source = "refExpectedReason.uuid", target = "expectedReasonId"),
            @Mapping(source = "refExpectedReason.text", target = "expectedReasonText"),
            @Mapping(source = "tblFailureInformationCondensed.uuid", target = "failureInformationCondensedId")
    })
    FailureInformationDto toFailureInformationDto(TblFailureInformation tblFailureInformation);

    @Mappings({
            @Mapping(target = "refFailureClassification.uuid", source = "failureClassificationId"),
            @Mapping(target = "refFailureClassification.classification", source = "failureClassification"),
            @Mapping(target = "refStatusIntern.uuid", source = "statusInternId"),
            @Mapping(target = "refStatusIntern.status", source = "statusIntern"),
            @Mapping(target = "refBranch.uuid", source = "branchId"),
            @Mapping(target = "refBranch.name", source = "branch"),
            @Mapping(target = "refBranch.description", source = "branchDescription"),
            @Mapping(target = "refBranch.colorCode", source = "branchColorCode"),
            @Mapping(target = "refRadius.uuid", source = "radiusId"),
            @Mapping(target = "refRadius.radius", source = "radius"),
            @Mapping(target = "refExpectedReason.uuid", source = "expectedReasonId"),
            @Mapping(target = "refExpectedReason.text", source = "expectedReasonText"),
            @Mapping(target = "tblFailureInformationCondensed.uuid", source = "failureInformationCondensedId")
    })
    TblFailureInformation toTblFailureInformation(FailureInformationDto failureInformationDto);

    FailureInformationDto mapForeignFiDtoToGfiDto(ForeignFailureDataDto foreignFailureDataDto);

    @AfterMapping
    default void populateStationIds(TblFailureInformation source, @MappingTarget FailureInformationDto failureInformationDto){
        List<TblStation> stations = source.getStations();

        failureInformationDto.setStationIds( stations != null ? stations.stream().map(TblStation::getUuid).collect(Collectors.toList()) : new LinkedList<>());
    }

    @AfterMapping
    default void splitFreetextAddress(TblFailureInformation source, @MappingTarget FailureInformationDto failureInformationDto){
        AddressSplitterMerger.splitFreetextAddress(source.getAddressType(), source.getCity(), source.getDistrict(),
                source.getPostcode(), failureInformationDto);
    }

    @AfterMapping
    default void splitFreetextAddress(FailureInformationDto source, @MappingTarget TblFailureInformation destTbl){
        AddressSplitterMerger.mergeFreetextAddress(source, destTbl);
    }

}
