/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.ImportService;
import org.eclipse.openk.gridfailureinformation.viewmodel.ImportDataDto;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Log4j2
@Component
public class ImportMessageConsumer {

    private final ImportService importService;

    public ImportMessageConsumer(ImportService importService) {
        this.importService = importService;
    }

    @RabbitListener(queues="${spring.rabbitmq.importQueue}")
    public void listenMessage(Message message) {
        try {
            if (Objects.nonNull(message.getMessageProperties().getHeaders().get("metaId"))) {
                importData(message);
            }
        } catch( Exception e ) {
            log.error("Unknown error while importing a message", e);
        }
    }

    public void importData (Message message) {
        ImportDataDto  importDataDto = new ImportDataDto();
        SimpleMessageConverter converter = new SimpleMessageConverter();

        importDataDto.setMessageContent( (String) converter.fromMessage(message));
        importDataDto.setMetaId((String) message.getMessageProperties().getHeaders().get("metaId"));
        importDataDto.setDescription((String) message.getMessageProperties().getHeaders().get("description"));
        importDataDto.setSource((String) message.getMessageProperties().getHeaders().get("source"));

        importService.validateAndImport(importDataDto);
    }
}
